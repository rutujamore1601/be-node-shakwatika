// Packages/files Imports
const express = require("express");
const cors = require("cors");
const config = require("./src/config/config");
const http = require("http");
const path = require('path');


// DB Connection
require("./src/config/dbConnection");

// Creating express server
const app = express();
const server = http.createServer(app);

//BodyParsing
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));


//CORS allow
app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "DELETE", "UPDATE", "PUT", "PATCH"],
  })
);

// Serve uploaded files statically
app.use('/assets', express.static(path.join(__dirname, 'uploads')));

// Handling routes request
app.get("/", (req, res) =>
  res.json({
    hello: "hi!",
  })
);

// Importing all the routes
require("./src/config/routeRegistry")(app);

// Listen Server
server.listen(config.app.port, () => {
  console.log(
    `### Server is running on port ${config.app.port} - ${process.env.NODE_ENV}`
  );
});

// Socket implementation for reference
require("./src/shared/utils/socket.util").ioSocket(server);