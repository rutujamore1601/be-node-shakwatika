const router = require("express").Router();
const downloadLinkController = require("./downloadLink.controller");

// Create Operation - Create downloadLink
router.post("/createDownloadLink", downloadLinkController.createDownloadLink);

// Read Operation - Get all downloadLink
router.post("/getAllDownloadLink", downloadLinkController.getAllDownloadLink);

// Read Operation - Get all downloadLink by Id
router.post(
  "/getAllDownloadLinkById/:id",
  downloadLinkController.getAllDownloadLinkById
);
router.get(
  "/getAllDownloadLinkById/:userId",
  downloadLinkController.getAllDownloadLinkById
);

// Read Operation - Get a single downloadLink by Id
router.get(
  "/getDownloadLinkById/:id",
  downloadLinkController.getDownloadLinkById
);

// Update Operation - Update downloadLink
router.put(
  "/updateDownloadLink/:id",
  downloadLinkController.updateDownloadLink
);

// Delete Operation - Delete downloadLink
router.delete(
  "/deleteDownloadLink/:id",
  downloadLinkController.deleteDownloadLink
);

module.exports = router;
