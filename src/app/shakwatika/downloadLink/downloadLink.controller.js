const downloadLinkModel = require("./downloadLink.model");

// Create Operation - Create downloadLink
const createDownloadLink = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  const downloadLink = new downloadLinkModel(req.body);
  downloadLink
    .save()
    .then((data) => {
      res.status(200).send({
        data: data,
        dataCount: data.length,
        message: "downloadLink created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the downloadLink.",
      });
    });
};

// Read Operation - Get all downloadLink
const getAllDownloadLink = async (req, res) => {
  try {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 25;
    const query = buildQuery(req.body);

    const [data, totalData] = await Promise.all([
      downloadLinkModel
        .find(query)
        .populate({
          path: "courseId",
        })
        .lean()
        .skip((page - 1) * limit)
        .limit(limit),
      downloadLinkModel.countDocuments(query),
    ]);

    if (data.length > 0) {
      res.status(200).send({
        data: data,
        message: "downloadLink fetched successfully!",
        success: true,
        statusCode: 200,
        totalData: totalData,
      });
    } else {
      res.status(200).send({
        data: [],
        message: "downloadLink not found!",
        success: false,
        statusCode: 200,
        totalData: 0,
      });
    }
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving the downloadLink.",
    });
  }
};

// Read Operation - Get all downloadLink by Id
const getAllDownloadLinkById = async (req, res) => {
  try {
    const id = req.params.id;
    const condition = { courseId: id };
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 25;
    const query = buildQuery(req.body);

    const [data, totalData] = await Promise.all([
      downloadLinkModel
        .find({ ...condition, ...query })
        .populate({
          path: "courseId",
        })
        .lean()
        .skip((page - 1) * limit)
        .limit(limit),
      downloadLinkModel.countDocuments({ ...condition, ...query }),
    ]);

    if (data.length > 0) {
      res.status(200).send({
        data: data,
        dataCount: totalData,
        message: "Download links fetched successfully!",
        success: true,
        statusCode: 200,
      });
    } else {
      res.status(404).send({
        data: [],
        dataCount: 0,
        message: "Download links not found!",
        success: false,
        statusCode: 404, // Use 404 for "not found" instead of 200
      });
    }
  } catch (err) {
    res.status(500).send({
      message:
        err.message ||
        "Some error occurred while retrieving the download links.",
    });
  }
};



// Read Operation - Get a single downloadLink by Id
const getDownloadLinkById = (req, res) => {
  const id = req.params.id;
  downloadLinkModel
    .findById(id)
    .populate({path:"courseId"})
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "downloadLink fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "downloadLink not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the downloadLink.",
      });
    });
};

// Update Operation - Update downloadLink
const updateDownloadLink = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  downloadLinkModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "downloadLink was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update downloadLink with order ID=" +
            id +
            ". Maybe downloadLink was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating downloadLink with ID=" + id,
      });
    });
};

// Delete Operation - Delete downloadLink
const deleteDownloadLink = (req, res) => {
  const id = req.params.id;

  downloadLinkModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "downloadLink was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete downloadLink with ID=" +
            id +
            ". Maybe downloadLink was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createDownloadLink,
  getAllDownloadLink,
  getAllDownloadLinkById,
  getDownloadLinkById,
  updateDownloadLink,
  deleteDownloadLink,
};


// Function to build the query dynamically
const buildQuery = (body) => {
  const filters = {
    syllabusName: "syllabusName",
  };

  const query = {};

  for (const key in filters) {
    if (body[filters[key]] && body[filters[key]].length > 0) {
      query[key] = key === "syllabusName"
        ? { $regex: body[filters[key]], $options: "i" }
        : { $in: body[filters[key]] };
    }
  }
  return query;
};
