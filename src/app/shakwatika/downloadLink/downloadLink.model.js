const mongoose = require("mongoose");

const downloadLinkSchema = new mongoose.Schema(
  {
    courseId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "courses",
      required: false,
    },
    syllabusLink: { type: String, required: false },
    syllabusName: { type: String, required: false }
  },
  { timestamps: true }
);
module.exports = mongoose.model("downloadLink", downloadLinkSchema);
