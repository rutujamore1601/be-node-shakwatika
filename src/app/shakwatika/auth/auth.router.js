const router = require("express").Router();
const authController = require("./auth.controller");
const authenticateToken = require("../../../shared/middleware/auth");

// Create Operation - Create auth
router.post("/createAuth", authController.createAuth);
router.post("/verifyAuthOTP", authController.verifyAuthOTP);
router.post("/createAuthUser", authController.createAuthUser);
router.post("/login", authController.login);
router.post("/verifyLoginOTP", authController.verifyLoginOTP);
router.get("/logout", authController.logout);

// Read Operation - Get all auth
router.get("/getAllAuth", authController.getAllAuth);

// // Read Operation - Get a single auth by Id
router.get("/getAuthById/:id", authController.getAuthById);

// // Update Operation - Update auth
router.put("/updateAuth/:id", authController.updateAuth);

// // Delete Operation - Delete auth
router.delete("/deleteAuth/:id", authController.deleteAuth);

router.post("/adminLogin", authController.adminLogin);

module.exports = router;
