const authModel = require("./auth.model");
const commonUtils = require("../../../shared/utils/common.util");
const downloadLinkModel = require("../downloadLink/downloadLink.model");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const sendOtp = (req, res) => {};

const generateOTP = () => {
  const min = 1000; // Minimum 4-digit number
  const max = 9999; // Maximum 4-digit number
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const createAuth = (req, res) => {
  if (!req.body.mobileNo) {
    return res.status(400).send({ message: "Mobile number is required!" });
  }

  const otp = generateOTP();
  req.body.token = jwt.sign({ mobileNo: req.body.mobileNo }, "HS256", {
    expiresIn: "2h",
  });
  req.body.otp = otp;

  let existingAuth;

  authModel.findOne({ mobileNo: req.body.mobileNo })
    .then((existingAuth) => {
      if (existingAuth) {
        existingAuth.otp = otp;
        return existingAuth.save();
      } else {
        const auth = new authModel(req.body);
        return auth.save();
      }
    })
    .then((data) => {
      res.status(200).send({
        data: data,
        message: existingAuth ? "Auth OTP updated successfully!" : "Auth created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating/updating the auth.",
      });
    });
};

const verifyAuthOTP = (req, res) => {
  const { mobileNo, otp } = req.body;

  if (!mobileNo || !otp) {
    return res.status(400).send({ message: "Mobile number and OTP are required." });
  }

  authModel.findOne({ mobileNo }).exec((err, data) => {
    console.log('data', data)
    if (err) {
      return res.status(500).send({
        message: "Internal Server Error",
        success: false,
        statusCode: 500,
      });
    }

    if (!data) {
      return res.status(404).send({
        message: "User not found.",
        success: false,
        statusCode: 404,
      });
    }

    if (data.otp !== otp) {
      return res.status(400).send({
        message: "Invalid OTP.",
        success: false,
        statusCode: 400,
      });
    }
    const token = jwt.sign({ mobileNo }, process.env.JWT_SECRET_KEY, {
      expiresIn: "2h",
    });

    data.token = token;
    data.save();

    return res.status(200).send({
      message: "OTP verified successfully.",
      token: token,
      success: true,
      statusCode: 200,
    });
  });
};

const createAuthUser = (req, res) => {
  if (!req.body) {
    return res.status(400).send({ message: "Content can not be empty!" });
  }
  const { email, mobileNo } = req.body;

  req.body.token = jwt.sign({ email }, "HS256", {
    expiresIn: "2h",
  });

  let existingAuth;

  authModel.findOne({ mobileNo })
    .then((existingAuth) => {
      if (existingAuth) {
        existingAuth.set(req.body);
        return existingAuth.save();
      } else {
        const auth = new authModel(req.body);
        return auth.save();
      }
    })
    .then((data) => {
      res.status(200).send({
        data: data,
        message: existingAuth ? "Auth updated successfully!" : "Auth created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating/updating the auth.",
      });
    });
};


const getAllAuth = (req, res) => {
  authModel
    .find()
    .select("-password")
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "auth fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "auth not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while Retrieve the auth.",
      });
    });
};

const getAllAuthById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  authModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "auth fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "auth not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while Retrieve the auth.",
      });
    });
};

// Read Operation - Get a single auth by Id
const getAuthById = (req, res) => {
  const id = req.params.id;
  authModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "auth fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "auth not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while Retrieve the auth.",
      });
    });
};

// Update Operation - Update auth
const updateAuth = async (req, res) => {
  try {
    if (!req.body) {
      return res.status(400).send({
        message: "Data to update can not be empty!",
      });
    }

    const id = req.params.id;

    const data = await authModel
      .findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true });

    if (data) {
      res.status(200).send({
        data: data,
        message: "auth was updated successfully.",
        success: true,
        statusCode: 200,
      });
    } else {
      res.status(404).send({
        message: `Cannot update auth with order ID=${id}. Maybe auth was not found!`,
        success: false,
        statusCode: 404,
      });
    }
  } catch (err) {
    console.error(err); // Log the error for debugging purposes
    res.status(500).send({
      message: `Error updating auth with ID=${req.params.id}`,
    });
  }
};

// Delete Operation - Delete auth
const deleteAuth = (req, res) => {
  const id = req.params.id;

  authModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "auth was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete auth with ID=" + id + ". Maybe auth was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

const login = (req, res) => {
  if (!req.body.mobileNo) {
    return res.status(400).send({ message: "Mobile number is required!" });
  }

  const otp = generateOTP();
  req.body.token = jwt.sign({ mobileNo: req.body.mobileNo }, "HS256", {
    expiresIn: "2h",
  });
  req.body.otp = otp;
  let existingAuth;

  authModel.findOne({ mobileNo: req.body.mobileNo })
    .then((existingAuth) => {
      if (existingAuth) {
        existingAuth.otp = otp;
        return existingAuth.save();
      } else {
        return res.status(404).send({ message: "Mobile number not found!" });
      }
    })
    .then((data) => {
      res.status(200).send({
        Otp: data.otp,
        message: existingAuth ? "Auth OTP updated successfully!" : "Auth created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating/updating the auth.",
      });
    });
};

const verifyLoginOTP = (req, res) => {
  const { mobileNo, otp } = req.body;

  if (!mobileNo || !otp) {
    return res.status(400).send({ message: "Mobile number and OTP are required." });
  }

  authModel.findOne({ mobileNo }).exec((err, data) => {
    if (err) {
      return res.status(500).send({
        message: "Internal Server Error",
        success: false,
        statusCode: 500,
      });
    }

    if (!data) {
      return res.status(404).send({
        message: "User not found.",
        success: false,
        statusCode: 404,
      });
    }

    if (data.otp !== otp) {
      return res.status(400).send({
        message: "Invalid OTP.",
        success: false,
        statusCode: 400,
      });
    }

    const token = jwt.sign({ mobileNo }, process.env.JWT_SECRET_KEY, {
      expiresIn: "2h",
    });

    data.token = token;
    data.save();

    return res.status(200).send({
      message: "OTP verified successfully.",
      token: token,
      success: true,
      statusCode: 200,
    });
  });
};

const adminLogin = (req, res) => {
  if (!req.body.mobileNo) {
    res
      .status(400)
      .send({ message: "Mobile number and password are required!" });
    return;
  }

  let loginData = {
    mobileNo: req.body.mobileNo,
    newOtp: generateOTP(req.body.mobileNo),
  };

  authModel
    .findOne(loginData)
    .then((data) => {
      if (data && (data.userRole === "1" || data.userRole === "2")) {
        res.status(200).send({
          message: "newOtp generated successfully!",
          newOtp: loginData.newOtp, // Use the newOtp from loginData object
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: "Invalid Credentials or User not authorized!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      console.log("err", err);
      res.status(500).send({
        message:
          err.message || "Some error occurred while logging in the User.",
      });
    });
};

const logout = (req, res) => {
  res.status(200).send({
    message: "Logout successful!",
    success: true,
    statusCode: 200,
  });
};

module.exports = {
  createAuth,
  getAllAuth,
  getAllAuthById,
  getAuthById,
  updateAuth,
  deleteAuth,
  login,
  adminLogin,
  sendOtp,
  logout,
  createAuthUser,
  verifyLoginOTP,
  verifyAuthOTP
};
