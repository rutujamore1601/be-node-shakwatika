const mongoose = require("mongoose");

const authSchema = new mongoose.Schema(
  {
    firstName: { type: String, required: false },
    lastName: { type: String, required: false },
    email: { type: String, required: false },
    password: { type: String, required: false },
    mobileNo: { type: Number, required: false },
    roleId: { type: String, required: false, default: 0 },
    otp: { type: String, required: false},
    status: { type: String, required: false },
    token: { type: String, required: false },
    profileImage: { type: String, required: false },
    deviceRegistrationToken: { type: String, required: false }
  },
  { timestamps: true }
);

module.exports = mongoose.model("auth", authSchema);
