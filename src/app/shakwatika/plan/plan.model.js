const mongoose = require("mongoose");

const planSchema = new mongoose.Schema(
  {
    planName: { type: String, required: false },
    planDate: { type: String, required: false },
    planPrice: { type: Number, required: false },
    // planImg: { type: String, required: false },
    streamId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "stream",
      required: true,
    },
    courseId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "courses",
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("plan", planSchema);
