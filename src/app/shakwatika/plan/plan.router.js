
const router = require("express").Router();
const planController = require('./plan.controller');
const authenticateToken = require("../../../shared/middleware/auth");

// Create Operation - Create plan
router.post('/createplan', planController.createplan);

// Read Operation - Get all plan
router.get('/getAllplan', planController.getAllplan);

// Read Operation - Get all plan by Id 
router.get("/getAllplanByStramId/:courseId/:stramId", planController.getAllplanByStramId);

// Read Operation - Get a single plan by Id
router.get('/getplanByStreamId/:courseId/:userId', authenticateToken,planController.getplanByStreamId);

// Update Operation - Update plan
router.put('/updateplan/:id', planController.updateplan);

// Delete Operation - Delete plan
router.delete('/deleteplan/:id', planController.deleteplan);

module.exports = router;
