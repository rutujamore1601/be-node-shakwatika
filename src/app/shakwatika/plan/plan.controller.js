const planModel = require("./plan.model");
const OrderModel = require("../purchasePlan/purchasePlan.model");

// Create Operation - Create plan
const createplan = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  const plan = new planModel(req.body);
  plan
    .save()
    .then((data) => {
      res.status(200).send({
        data: data,
        dataCount: data.length,
        message: "plan created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the plan.",
      });
    });
};

// Read Operation - Get all plan
const getAllplan = (req, res) => {
  planModel
    .find()
    .populate({ path: "streamId",select :'streamName' })
    .populate({ path: "courseId",select :'coursesName' })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "plan fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "plan not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the plan.",
      });
    });
};

// Read Operation - Get all plan by Id
const getAllplanById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  planModel
    .find(condition)
    .populate({path:"streamId"})
    .populate({path:"courseId"})
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "plan fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "plan not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the plan.",
      });
    });
};

// Read Operation - Get a single plan by Id
const getplanByStreamId = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const userId = req.params.userId;
    console.log('courseId', courseId);

    const streams = await streamModel.find({
      courseId: courseId,
    });

    if (!streams || streams.length === 0) {
      return res.status(200).send({
        data: [],
        dataCount: 0,
        message: "Streams not found for the given courseId!",
        success: false,
        statusCode: 200,
      });
    }

    const planPromises = streams.map(async (stream) => {
      const plans = await planModel
        .find({ streamId: stream._id })
        .populate({ path: "streamId" })
        .populate({ path: "courseId" });

      const plansWithPurchaseInfo = await Promise.all(
        plans.map(async (plan) => {
          const payInfo = await OrderModel.findOne({
            $and: [
              { planId: plan._id },
              { userId: userId }
            ]
          });
          console.log('payInfo', payInfo); // For debugging
          const isPurchased = payInfo ? true : false;
          return { ...plan.toObject(), isPurchased };
        })
      );

      return plansWithPurchaseInfo;
    });

    const results = await Promise.all(planPromises);
    const mergedPlans = [].concat(...results);

    if (mergedPlans.length > 0) {
      const plansArray = mergedPlans.reduce((acc, val) => acc.concat(val), []); // flatten the array
      res.status(200).send({
        data: plansArray,
        dataCount: plansArray.length,
        message: "Plans fetched successfully!",
        success: true,
        statusCode: 200,
      });
    } else {
      res.status(200).send({
        data: [],
        dataCount: 0,
        message: "Plans not found!",
        success: false,
        statusCode: 200,
      });
    }
  } catch (error) {
    console.log("error", error);
    res.status(500).json("Internal Server Error");
  }
};




const getAllplanByStramId = (req, res) => {
  const courseId = req.params.courseId;
  const stramId = req.params.stramId;
  const userId = req.params.userId;

  planModel.findOne({courseId,stramId})
  .populate({path:"streamId"})
  .populate({path:"courseId"})
  .lean()
    .then((data) => {
    OrderModel.findOne({$and: [
      {planId:data._id},
      {userId:userId}
    ] }) .then((payInfo) => {
      if(payInfo){
        data.isPurchased = true;
      }else{
        data.isPurchased = false;
      }
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: 1,
          message: "plan fetched successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: null,
          dataCount: 0,
          message: 'plan not found with courseId=' + courseId,
          success: false,
          statusCode: 200,
        });
      }
    })

    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving the plan.",
        success: false,
        statusCode: 500,
      });
    });
};

// Read Operation - Get a single subject by ClassId
const getplanBySubject = (req, res) => {
  const subject = req.params.subject;
  planModel
    .findOne({ subject: subject })
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: 1,
          message: "plan fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "plan not found with ID=" + subject,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the plan.",
      });
    });
};

// Update Operation - Update plan
const updateplan = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  planModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "plan was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update plan with order ID=" +
            id +
            ". Maybe plan was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating plan with ID=" + id,
      });
    });
};

// Delete Operation - Delete plan
const deleteplan = (req, res) => {
  const id = req.params.id;

  planModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "plan was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete plan with ID=" +
            id +
            ". Maybe plan was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createplan,
  getAllplan,
  getAllplanById,
  getplanByStreamId,
  updateplan,
  deleteplan,
  getplanBySubject,
  getAllplanByStramId,
};
