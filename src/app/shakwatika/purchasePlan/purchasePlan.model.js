const mongoose = require("mongoose");

const purchasePlanSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "auth",
      required: true,
    },
    planId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "plan",
      required: true,
    },
    razorpayResData: { type: Object, required: false },
    paymentStatus: { type: String, required: true },
    orderStatus: { type: String, required: true },
    totalAmount: { type: Number, required: true },
    planPrice: { type: Number, required: true },
    purchase: { type: String, default: false },
    status: { type: String, required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("purchasePlan", purchasePlanSchema);
