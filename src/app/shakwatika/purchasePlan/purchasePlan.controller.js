const purchasePlanModel = require("./purchasePlan.model");
const PlanModel = require("../plan/plan.model");
const razorpayPaymentUtil = require("../../../shared/utils/razorpayPayment.util");
const moment = require("moment");

const proceedPayment = (req, res) => {
  razorpayPaymentUtil.proceedPayment(req, res);
};

// Create Operation - Create purchasePlan
const createPurchasePlan = async (req, res) => {
  if (razorpayPaymentUtil.verifyPaymentSignature(req.body.razorpayResData)) {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }

    const status = "started";

    const purchasePlanData = { ...req.body, status };

    const purchasePlan = new purchasePlanModel(purchasePlanData);

    if (req.body.paymentStatus === "Paid") {
      purchasePlan.purchase = true;
    } else {
      purchasePlan.purchase = false;
    }

    purchasePlan
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "purchasePlan created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message ||
            "Some error occurred while creating the purchasePlan.",
        });
      });
  } else {
    console.log("Signature verification failed.");
  }
};
// Read Operation - Get all purchasePlan
const getAllPurchasePlan = (req, res) => {
  purchasePlanModel
    .find()
    .populate({path:"userId", select: 'userName mobileNo'})
    .populate({
      path: "planId", select: 'planName',
      populate: {
        path: "courseId",select :'coursesName'
      }
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "purchasePlan fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "purchasePlan not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the purchasePlan.",
      });
    });
};

// Read Operation - Get all purchasePlan by Id
const getAllPurchasePlanById = (req, res) => {
  const userId = req.params.userId;
  const condition = { userId: userId };

  purchasePlanModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "purchasePlan fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "purchasePlan not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the purchasePlan.",
      });
    });
};

// Read Operation - Get a single purchasePlan by Id
const getPurchasePlanById = (req, res) => {
  const id = req.params.id;
  purchasePlanModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "purchasePlan fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "purchasePlan not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the purchasePlan.",
      });
    });
};

// Update Operation - Update purchasePlan
const updatePurchasePlan = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  purchasePlanModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "purchasePlan was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update purchasePlan with purchasePlan ID=" +
            id +
            ". Maybe purchasePlan was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating purchasePlan with ID=" + id,
      });
    });
};

// Delete Operation - Delete purchasePlan
const deletePurchasePlan = (req, res) => {
  const id = req.params.id;

  purchasePlanModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "purchasePlan was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete purchasePlan with ID=" +
            id +
            ". Maybe purchasePlan was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createPurchasePlan,
  getAllPurchasePlan,
  getAllPurchasePlanById,
  getPurchasePlanById,
  updatePurchasePlan,
  deletePurchasePlan,
  proceedPayment,
};
