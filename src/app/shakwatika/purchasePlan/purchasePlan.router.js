
const router = require("express").Router();
const purchasePlanController = require('./purchasePlan.controller');

// Create Operation - Create purchasePlan
router.post('/createPurchasePlan', purchasePlanController.createPurchasePlan);

router.post('/proceedPayment', purchasePlanController.proceedPayment);

// Read Operation - Get all purchasePlan
router.get('/getAllPurchasePlan', purchasePlanController.getAllPurchasePlan);

// Read Operation - Get all purchasePlan by Id 
router.get("/getAllPurchasePlanById/:userId", purchasePlanController.getAllPurchasePlanById);

// Read Operation - Get a single purchasePlan by Id
router.get('/getPurchasePlanById/:id', purchasePlanController.getPurchasePlanById);

// Update Operation - Update purchasePlan
router.put('/updatePurchasePlan/:id', purchasePlanController.updatePurchasePlan);

// Delete Operation - Delete purchasePlan
router.delete('/deletePurchasePlan/:id', purchasePlanController.deletePurchasePlan);

module.exports = router;
