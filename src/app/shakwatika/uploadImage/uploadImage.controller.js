const uploadImageModel = require("./uploadImage.model");
const commonUtil = require("../../../shared/utils/common.util");
const fileUploadUtil = require("../../../shared/utils/fileUpload.util");
const cloudinary = require('cloudinary').v2;
const axios = require('axios');

// Configure Cloudinary
cloudinary.config({
  cloud_name: 'dnckyhfb0',
  api_key: '144151894176496',
  api_secret: 'mOsuLT-U_eSlfz7u-22kvlfu5pI',
});


const uploadImage = async (req, res) => {
  const file = req.file;

  if (!file) {
    return res.status(400).send('No file uploaded.');
  }

  try {
    const base64Data = file.buffer.toString('base64');

    const response = await axios.post('https://api.imgbb.com/1/upload', {
      key: '855f0d86092bb4d7b7c4f4750e58f406',
      image: base64Data,
    }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });

    if (response.status === 200) {
      const imageUrl = response.data.data.url;
      const apiResponse = {
        data: {
          image: imageUrl,
        },
        message: 'uploadImage created successfully!',
        success: true,
        statusCode: 200,
      };
      res.status(200).json(apiResponse);
    } else {
      res.status(500).json({
        message: 'Error uploading file to Imgbb.',
        success: false,
        statusCode: 500,
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: 'Error uploading file to Imgbb.',
      success: false,
      statusCode: 500,
    });
  }
};

// Define a route to handle image uploads
// const uploadImage = async (req, res) => {
//   const file = req.file;

//   if (!file) {
//     return res.status(400).send('No file uploaded.');
//   }

//   try {
//     const base64Data = file.buffer.toString('base64');

//     const result = await cloudinary.uploader.upload(`data:${file.mimetype};base64,${base64Data}`, {
//       folder: 'uploads',
//       use_filename: true,
//     });

//     res.status(200).json({ imageUrl: result.secure_url });
//   } catch (error) {
//     console.error(error);
//     res.status(500).send('Error uploading file to Cloudinary.');
//   }
// };

// Create Operation - Create uploadImage
const createUploadImage = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  const folderName = "uploadImg";

  fileUploadUtil.uploadFile(folderName, "image")(req, res, async () => {
    req.body.image = `${commonUtil.getFileUrlPath(folderName)}/${req.body.image}`;

    const uploadImage = new uploadImageModel(req.body);
    uploadImage
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "uploadImage created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message ||
            "Some error occurred while creating the uploadImage.",
        });
      });
  });
};

const multipleUploadImage = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  const folderName = "uploadImg";

  fileUploadUtil.multipleUploadFile(folderName, "multipleImg")(
    req,
    res,
    async () => {
      const uploadedImageUrls = req.body.multipleImg.map((imageName) => {
        return `${commonUtil.getFileUrlPath(folderName)}/${imageName}`;
      });

      const uploadImage = new uploadImageModel({
        multipleImg: uploadedImageUrls,
      });

      uploadImage
        .save()
        .then((data) => {
          res.status(200).send({
            data: {
              multipleImg: data.multipleImg,
              _id: data._id,
              createdAt: data.createdAt,
              updatedAt: data.updatedAt,
              __v: data.__v,
            },
            dataCount: data.multipleImg.length,
            message: "uploadImage created successfully!",
            success: true,
            statusCode: 200,
          });
        })
        .catch((err) => {
          res.status(500).send({
            message:
              err.message ||
              "Some error occurred while creating the uploadImage.",
          });
        });
    }
  );
};

// Read Operation - Get all uploadImage
const getAllUploadImage = (req, res) => {
  uploadImageModel
    .find()
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "uploadImage fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "uploadImage not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the uploadImage.",
      });
    });
};

// Read Operation - Get all uploadImage by Id
const getAllUploadImageById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  uploadImageModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "uploadImage fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "uploadImage not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the uploadImage.",
      });
    });
};

// Read Operation - Get a single uploadImage by Id
const getUploadImageById = (req, res) => {
  const id = req.params.id;
  uploadImageModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "uploadImage fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "uploadImage not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the uploadImage.",
      });
    });
};

// Update Operation - Update uploadImage
const updateUploadImage = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  uploadImageModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "uploadImage was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update uploadImage with order ID=" +
            id +
            ". Maybe uploadImage was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating uploadImage with ID=" + id,
      });
    });
};

// Delete Operation - Delete uploadImage
const deleteUploadImage = (req, res) => {
  const id = req.params.id;

  uploadImageModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "uploadImage was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete uploadImage with ID=" +
            id +
            ". Maybe uploadImage was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createUploadImage,
  multipleUploadImage,
  getAllUploadImage,
  getAllUploadImageById,
  getUploadImageById,
  updateUploadImage,
  deleteUploadImage,
  uploadImage
};
