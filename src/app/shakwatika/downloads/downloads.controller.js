
const downloadsModel = require('./downloads.model');

  // Create Operation - Create downloads
  const createDownloads = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const downloads = new downloadsModel(req.body);
    downloads
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "downloads created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the downloads.",
        });
      });
  };

  // Read Operation - Get all downloads
  const getAllDownloads = (req, res) => {
    downloadsModel.find()
    .populate({path:"userId"})
    .populate({path:"downloadId"})
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "downloads fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "downloads not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the downloads.",
        });
      });
  };

  // Read Operation - Get all downloads by Id 
  const getAllDownloadsById  = (req, res) => {
    const userId = req.params.userId;

    downloadsModel.find({userId})
    .populate({path:"userId"})
    .populate({path:"downloadId"})
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "downloads fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "downloads not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the downloads.",
        });
      });
  };

  // Read Operation - Get a single downloads by Id
  const getDownloadsById = (req, res) => {
    const id = req.params.id;
    downloadsModel.findById(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "downloads fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'downloads not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the downloads.",
        });
      });
  };

  // Update Operation - Update downloads
 const updateDownloads = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  downloadsModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "downloads was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update downloads with order ID=' + id + '. Maybe downloads was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating downloads with ID=" + id,
      });
    });
};


// Delete Operation - Delete downloads
  const deleteDownloads = (req, res) => {
    const userId = req.params.userId;
    const downloadId = req.params.downloadId;
    const condition = {
      userId: userId,
      downloadId: downloadId,
    };
    downloadsModel.deleteOne(condition)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "downloads was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete downloads with ID=' + id + '. Maybe downloads was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createDownloads,
  getAllDownloads,
  getAllDownloadsById,
  getDownloadsById,
  updateDownloads,
  deleteDownloads
};
