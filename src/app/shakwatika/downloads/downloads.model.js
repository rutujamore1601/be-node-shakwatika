
const mongoose = require('mongoose');

const downloadsSchema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: "auth", required: false },
  downloadId: { type: mongoose.Schema.Types.ObjectId, ref: "downloadLink", required: false },
}, { timestamps: true });

module.exports = mongoose.model('downloads', downloadsSchema);
