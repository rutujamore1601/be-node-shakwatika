
const router = require("express").Router();
const downloadsController = require('./downloads.controller');

// Create Operation - Create downloads
router.post('/createDownloads', downloadsController.createDownloads);

// Read Operation - Get all downloads
router.get('/getAllDownloads', downloadsController.getAllDownloads);

// Read Operation - Get all downloads by Id 
router.get("/getAllDownloadsById/:userId", downloadsController.getAllDownloadsById);

// Read Operation - Get a single downloads by Id
router.get('/getDownloadsById/:id', downloadsController.getDownloadsById);

// Update Operation - Update downloads
router.put('/updateDownloads/:id', downloadsController.updateDownloads);

// Delete Operation - Delete downloads
router.delete('/deleteDownloads/:userId/:downloadId', downloadsController.deleteDownloads);

module.exports = router;
