const shakwatika = (app) => {
  // Add your middleware here if needed
  app.use("/auth", require("../app/shakwatika/auth/auth.router"));

  app.use(
    "/uploadImage",
    require("../app/shakwatika/uploadImage/uploadImage.router")
  );
  app.use("/plan", require("../app/shakwatika/plan/plan.router"));
  app.use(
    "/purchasePlan",
    require("../app/shakwatika/purchasePlan/purchasePlan.router")
  );
  app.use(
    "/downloadLink",
    require("../app/shakwatika/downloadLink/downloadLink.router")
  );
  app.use("/downloads", require("../app/shakwatika/downloads/downloads.router"));
};

const routes = {
  shakwatika,
};

module.exports = routes[process.env.PROJ_NAME];
