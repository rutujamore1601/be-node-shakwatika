const jwt = require("jsonwebtoken");
const UserModel = require('../../app/shakwatika/auth/auth.model');

// Secret key to sign the token
const secretKey = process.env.JWT_SECRET_KEY;

// Middleware function for JWT authentication
const authenticateToken = (req, res, next) => {
  // Get the authorization header from the request
  const authHeader = req.headers["authorization"];

  // Extract the token from the header
  const token = authHeader && authHeader.split(" ")[1];

  // If there's no token, return an error response
  if (!token) {
    return res.status(401).json({ message: "No token provided" });
  }

  // Verify the token with the secret key
  jwt.verify(token, secretKey, (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        return res.status(401).json({ message: "Token has expired" });
      } else {
        console.error("Error verifying token:", err);
        return res.status(401).json({ message: "Token is not valid" });
      }
    }

    // Check if the token exists in the database
    UserModel.findOne({ mobileNo: decoded.mobileNo, token }, (err, user) => {
      if (err) {
        console.error("Error finding user in database:", err);
        return res.status(401).json({ message: "Error finding user in database" });
      }

      if (!user) {
        console.error("Token not found in database or user not found");
        return res.status(401).json({ message: "Invalid token" });
      }
      
      // Token is valid and found in the database, attach the user to the request object
      req.user = user;
      next();
    });
  });
};

module.exports = authenticateToken;
